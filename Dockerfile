FROM python:3.9-slim

# COPY requirements.txt /opt/Sprinter/requirements.txt
# RUN apt-get update && \
#     apt-get install --no-install-recommends -y gcc
# RUN pip install -r /opt/Sprinter/requirements.txt
# RUN apt-get remove --purge -y gcc && \
#     apt-get autoremove --purge -y && \
#     rm -rf /var/lib/apt/lists/*
RUN adduser --no-create-home --disabled-password --uid 7894 --system runuser
COPY . /opt/recomengine
RUN chown -R 7894 /opt/recomengine
USER runuser
WORKDIR /opt/recomengine

ENTRYPOINT [ "python", "main.py" ]
