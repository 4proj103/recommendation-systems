from datetime import datetime

histCom = [
    [{"Product": "Produit 1",
      "Coarse Class Name (str)": "Apple",
      "nbr": 3},
     {"Product": "Porduit 2",
      "Coarse Class Name (str)": "nn",
      "nbr": 4},
     {"Product": "Porduit 3",
      "Coarse Class Name (str)": "Apple",
      "nbr": 15}],
    [{"Product": "Produit 4",
      "Coarse Class Name (str)": "Apple",
      "nbr": 13},
     {"Product": "Porduit 5",
      "Coarse Class Name (str)": "Apple",
      "nbr": 14},
     {"Product": "Porduit 6",
      "Coarse Class Name (str)": "Apple",
      "nbr": 60}],
    [{"Product": "Produit 1",
      "Coarse Class Name (str)": "Apple",
      "nbr": 100},
     {"Product": "Porduit 2",
      "Coarse Class Name (str)": "nn",
      "nbr": 240},
     {"Product": "Porduit 3",
      "Coarse Class Name (str)": "Apple",
      "nbr": 25}],
    [{"Product": "Produit 4",
      "Coarse Class Name (str)": "Apple",
      "nbr": 3},
     {"Product": "Porduit 5",
      "Coarse Class Name (str)": "Apple",
      "nbr": 4},
     {"Product": "Porduit 6",
      "Coarse Class Name (str)": "Apple",
      "nbr": 1}],
    [{"Product": "Produit 1",
      "Coarse Class Name (str)": "nn",
      "nbr": 13},
     {"Product": "Porduit 2",
      "Coarse Class Name (str)": "Apple",
      "nbr": 14},
     {"Product": "Porduit 3",
      "Coarse Class Name (str)": "Apple",
      "nbr": 25}],
    [{"Product": "Produit 4",
      "Coarse Class Name (str)": "Apple",
      "nbr": 1},
     {"Product": "Porduit 5",
      "Coarse Class Name (str)": "Apple",
      "nbr": 240},
     {"Product": "Porduit 6",
      "Coarse Class Name (str)": "Apple",
      "nbr": 1}]
]

shelves = [
    [{
        '_id': '608fe193dd8fb70008828e98',
        'ray': 'r9',
        's1': [
            'product',
            4,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's2': [
            'product',
            6,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's3': [
            'product160399',
            6,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's4': [
            'product',
            2,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's5': [
            'product',
            8,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's6': [
            'product',
            7,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's7': [
            'product',
            7,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's8': [
            'product',
            8,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ]
    }],
    [{
        '_id': '608fe193dd8fb70008828e91',
        'ray': 'r10',
        's1': [
            'product',
            3,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's2': [
            'product',
            3,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's3': [
            'product',
            2,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's4': [
            'product',
            10,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's5': [
            'product',
            157,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's6': [
            'product',
            14,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's7': [
            'product',
            52,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's8': [
            'product',
            20,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ]
    }],
    [{
        '_id': '608fe193dd8fb70008828e91',
        'ray': 'r11',
        's1': [
            'product',
            1,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's2': [
            'product',
            15,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's3': [
            'product',
            16,
            'Coarse Class Name (str)',
            '2021-05-25',
            '12'
        ],
        's4': [
            'product2',
            15,
            'Coarse Class Name (str)',
            '2021-05-25',
            '112'
        ],
        's5': [
            'product3',
            14,
            'Apple',
            '2021-06-11',
            '2'
        ],
        's6': [
            'Produit 3',
            13,
            'Apple',
            '2021-06-27',
            '12'
        ],
        's7': [
            'product1',
            12,
            'Apple',
            '2021-05-29',
            '12'
        ],
        's8': [
            'product',
            10,
            'Apple',
            '2021-07-10',
            '12'
        ]
    }]
]

productSee = {
    "_id": '60853b7a0d5a0400071c19e8',
    "Class Name (str)": 'Golden-Delicious',
    "Class ID (int)": 0,
    "Coarse Class Name (str)": 'Apple',
    "Coarse Class ID (int)": 0,
    "Iconic Image Path (str)": '/iconic-images-and-descriptions/Fruit/Apple/Golden-Delicious/Golden-Delicious_Iconic.jpg',
    "Product Description Path (str)": '/iconic-images-and-descriptions/Fruit/Apple/Golden-Delicious/Golden-Delicious_Description.txt'
}

panier = [
    {
        "Product": "Produit 300",
        "Coarse Class Name (str)": "Apple",
        "nbr": 1,
        "rayon": 'r9'
    },
    {
        "Product": "Porduit 24",
        "Coarse Class Name (str)": "Apple",
        "nbr": 240,
        "rayon": 'r9'
    },
    {
        "Product": "Porduit 14",
        "Coarse Class Name (str)": "nn",
        "nbr": 25,
        "rayon": 'r9'
    }
]

promotion = [
    {
        "Product": "Produit 300",
        "Coarse Class Name (str)": "Apple",
        "nbr": 1,
        "rayon": 'r9'
    },
    {
        "Product": "Porduit 24",
        "Coarse Class Name (str)": "Apple",
        "nbr": 240,
        "rayon": 'r9'
    },
    {
        "Product": "Porduit 12",
        "Coarse Class Name (str)": "nn",
        "nbr": 25,
        "rayon": 'r9'
    }
]

position = "r9"

# Le premier systeme va retourner les 5 produits que l'utilisateur à le plus acheté.


def first_system(histCom, productSee, panier):
    recommendation_list_int = []
    recommendation_list = []
    categorise_name = "Coarse Class Name (str)"
    product_categorise = productSee[categorise_name]
    for command in histCom:
        for product in command:
            if product[categorise_name] == product_categorise and product["Product"] != productSee["Class Name (str)"]:
                add = True
                for item in recommendation_list_int:
                    if item["Product"] == product["Product"]:
                        add = False
                        item['nbr'] += product['nbr']
                        break
                if add:
                    recommendation_list_int.append(product)
    recommendation_list_int = sorted(recommendation_list_int, reverse=True, key=lambda product: product["nbr"])
    x = 0
    add = True
    for item in recommendation_list_int:
        if x >= 5:
            break
        if len(recommendation_list) != 0:
            for element in recommendation_list:
                if item["Product"] == element["Product"]:
                    add = False
                    break
            if add:
                for article in panier:
                    if article["Product"] == item["Product"]:
                        add = False
                        break
        else:
            for article in panier:
                if article["Product"] == item["Product"]:
                    add = False
                    break
        if not add:
            add = True
            continue
        x += 1
        recommendation_list.append(item)
    return recommendation_list

# le second systeme va recommander 5 produits present dans les etageres en tenant compte de l'emprunte carbonne et de la date de peremption.


def second_system(recommendation_list, shelves, productSee, panier):
    recommendation_list_int = []
    categorise_name = "Coarse Class Name (str)"
    x = 0
    suite = True
    for etage in shelves:
        if x >= 5:
            break
        for product in etage:
            if x >= 5:
                break
            for i in range(1, len(product) - 1):
                number = 's' + str(i)
                if product[number][0] != productSee["Class Name (str)"] and product[number][2] == productSee[categorise_name]:
                    for produit in panier:
                        if product[number][0] == produit['Product']:
                            suite = False
                            break
                    if suite:
                        for element in recommendation_list:
                            if product[number][0] == element["Product"]:
                                suite = False
                                break
                    if suite:
                        for item in recommendation_list_int:
                            if product[number][0] == item["Product"]:
                                suite = False
                                break

                    if not suite:
                        suite = True
                        continue
                    x += 1
                    date = product[number][3]
                    date = date.split("-")
                    score = datetime.now() - datetime(int(date[0]), int(date[1]), int(date[2]))
                    score = -score.days + int(product[number][4])
                    recommendation_list_int.append({
                        'Product': str(product[number][0]),
                        'Coarse Class Name (str)': str(product[number][2]),
                        'nbr': int(product[number][1]),
                        'score': score
                    })
    recommendation_list_int = sorted(recommendation_list_int, key=lambda product: product["score"])
    for item in recommendation_list_int:
        y = 0
        if y <= 5:
            recommendation_list.append(item)
            y += 1
    return recommendation_list

# Le 3eme systeme va recommandé 5 produits du rayon ou se trouve le client


def third_system(recommendation_list, productSee, panier, shelves, position):
    x = 0
    suite = True
    for etage in shelves:
        if x >= 5:
            break
        for product in etage:
            if x >= 5:
                break
            if product['ray'] == position:
                for i in range(1, len(product) - 1):
                    number = 's' + str(i)
                    if product[number][0] != productSee["Class Name (str)"]:
                        for produit in panier:
                            if product[number][0] == produit['Product']:
                                suite = False
                                break
                        if suite:
                            for element in recommendation_list:
                                if product[number][0] == element["Product"]:
                                    suite = False
                                    break
                        if not suite:
                            suite = True
                            continue
                        x += 1
                        recommendation_list.append({
                            'Product': str(product[number][0]),
                            'Coarse Class Name (str)': str(product[number][2]),
                            'nbr': int(product[number][1])})

    return recommendation_list

# Le dernier systeme va recommander 5 produits en promotion en fonction du rayon d'ou se trouve le client.


def fourth_system(recommendation_list, productSee, panier, promotion, position):
    suite = True
    x = 0
    for produit in promotion:
        if x <= 5:
            if produit["rayon"] == position and produit["Product"] != productSee["Class Name (str)"]:
                for element in panier:
                    if produit["Product"] == element["Product"]:
                        suite = False
                        break
                if suite:
                    for item in recommendation_list:
                        if produit["Product"] == item["Product"]:
                            suite = False
                            break
                if suite:
                    x += 1
                    recommendation_list.append(produit)
                suite = True
    return recommendation_list


recommendation_list = first_system(histCom, productSee, panier)
recommendation_list = second_system(recommendation_list, shelves, productSee, panier)
recommendation_list = third_system(recommendation_list, productSee, panier, shelves, position)
recommendation_list = fourth_system(recommendation_list, productSee, panier, promotion, position)

for item in recommendation_list:
    print(item)
